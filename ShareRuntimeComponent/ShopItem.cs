﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareRuntimeComponent
{
    public sealed class ShopItem
    {
        public string name { get; set; }
        public int price { get; set; }

        public string expireDate { get; set; }
    }
}
