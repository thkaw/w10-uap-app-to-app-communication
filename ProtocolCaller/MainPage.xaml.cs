﻿using Newtonsoft.Json;
using ShareRuntimeComponent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ProtocolCaller
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Debug.WriteLine("ProtocolCaller Package Family Name:" + Windows.ApplicationModel.Package.Current.Id.FamilyName);
        }

        private async void btn_launch_Click(object sender, RoutedEventArgs e)
        {

            //告訴系統我要找支援dxuapsample這個protocal的app起來接我要傳遞的SomeData內的test123(傳統透過URI資料傳送方式)
            await Windows.System.Launcher.LaunchUriAsync(new Uri("dxuapsample:?SomeData=test123"));
        }

        private async void btn_launch_1only_Click(object sender, RoutedEventArgs e)
        {
            //直接call FirstProtocolHandler起來, 方便你未來debug或者直接呼叫指定app(例如官方FB APP)起來
            var options = new Windows.System.LauncherOptions();
            options.TargetApplicationPackageFamilyName = "f9e0d8a8-1d6c-406c-acd1-ae2edbd4fc3f_mdyfezwv4mv6j";
            await Windows.System.Launcher.LaunchUriAsync(new Uri("dxuapsample:?SomeData=test123"), options);

        }

        private async void btn_launch_withdata_1only_Click(object sender, RoutedEventArgs e)
        {
            await SendItemsToFirstProtocolHandler();

        }

        private async Task SendItemsToFirstProtocolHandler()
        {
            var options = new Windows.System.LauncherOptions();
            options.TargetApplicationPackageFamilyName = "f9e0d8a8-1d6c-406c-acd1-ae2edbd4fc3f_mdyfezwv4mv6j";

            List<ShopItem> si_list = new List<ShopItem>()
            {
                new ShopItem() { name = "Apple", price = 10, expireDate = new DateTime(2015, 6, 30).ToString() },
                new ShopItem() { name = "Orange", price = 5, expireDate = new DateTime(2016, 6, 30).ToString() },
                new ShopItem() { name = "Banana", price = 2, expireDate = new DateTime(2015, 7, 30).ToString() },
                new ShopItem() { name = "Raspberry", price = 199, expireDate = new DateTime(2015, 6, 10).ToString() }
            };

            var serializedItems = JsonConvert.SerializeObject(si_list);


            ValueSet inputData = new ValueSet();

            inputData["Transaction"] = Guid.NewGuid().ToString();
            inputData["Items"] = serializedItems;

            var response = await Windows.System.Launcher.LaunchUriForResultsAsync(new Uri("dxuapsample:"), options, inputData);
            if (response.Status == Windows.System.LaunchUriStatus.Success)
            {
                var result = response.Result;

                if (result != null)
                {
                    MessageDialog md = new MessageDialog(result["Reason"].ToString());
                    md.ShowAsync();
                }
                else
                {
                    MessageDialog md = new MessageDialog("Did completed response!");
                    md.ShowAsync();
                }
            }
        }
    }
}
